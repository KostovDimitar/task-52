import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });
})

  document.body.addEventListener("click", () => {
    const article0 = document.createElement('article');
    article0.className = 'message';
    article0.innerText = 'Article0';
    document.body.appendChild(article0);

    const article1 = document.createElement('article');
    article1.className = 'message';
    article1.innerText = 'Article1';
    document.body.appendChild(article1);

    const article2 = document.createElement('article');
    article2.className = 'message';
    article2.innerText = 'Article2';
    document.body.appendChild(article2);

    const article3 = document.createElement('article');
    article3.className = 'message';
    article3.innerText = 'Article3';
    document.body.appendChild(article3);

    const article4 = document.createElement('article');
    article4.className = 'message';
    article4.innerText = 'Article4';
    document.body.appendChild(article4);
  })